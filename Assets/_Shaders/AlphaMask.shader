﻿Shader "AlphaMask" {
	Properties{
		_MainTex("Mask", 2D) = "white" {}
		_ColorTex("Color", 2D) = "white" {}
		_Color("Main Color", Color) = (1,1,1,1)
	}
		SubShader{
		Tags{ "RenderType" = "Transparent" "Queue" = "Transparent+1" }
		ZWrite On
		ZTest LEqual

		ColorMask RGB

		Blend SrcAlpha OneMinusSrcAlpha

		Pass{
			SetTexture[_MainTex]{ Combine texture }
			SetTexture[_ColorTex]{ Combine texture, previous * texture}

			SetTexture[_ColorTex]{
				constantColor[_Color]
				Combine texture * constant, texture * constant
			}
		}
	}
}