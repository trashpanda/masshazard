﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Shaker : MonoBehaviour {

	public float duration = 0.5f;
	public float speed = 1.0f;
	public float magnitude = 0.1f;

	public Transform target;

	public bool test = false;
	public bool xZAxis = true;

	public void PlayShake() {

		StopAllCoroutines();
		StartCoroutine("Shake");
	}

	void Update() {
		if (test) {
			test = false;
			PlayShake();
		}
	}

	IEnumerator Shake() {

		float elapsed = 0.0f;

		Vector3 originalPosition = target.transform.position;
		float randomStart = Random.Range(-1000.0f, 1000.0f);

		while (elapsed < duration) {
			elapsed += Time.deltaTime;		

			float percentComplete = elapsed / duration;			

			// We want to reduce the shake from full power to 0 starting half way through
			float damper = 1.0f - Mathf.Clamp(2.0f * percentComplete - 1.0f, 0.0f, 1.0f);

			// Calculate the noise parameter starting randomly and going as fast as speed allows
			float alpha = randomStart + speed * percentComplete;

			// map noise to [-1, 1]
			//float x = Mathf.PerlinNoise(alpha, 0.0f, 0.0f) * 2.0f - 1.0f;
			//float y = Mathf.PerlinNoise(0.0f, alpha, 0.0f) * 2.0f - 1.0f;
			float x = Mathf.PerlinNoise(alpha, 0.0f) * 2.0f - 1.0f;
			float y = Mathf.PerlinNoise(alpha, 0.0f) * 2.0f - 1.0f;

			x *= magnitude * damper;
			y *= magnitude * damper;
			if (xZAxis)
				target.transform.position = new Vector3(x + originalPosition.x, originalPosition.y, y + originalPosition.z);
			else
				target.transform.position = new Vector3(x + originalPosition.x, y + originalPosition.y, originalPosition.z);
			yield return null;
		}

		target.transform.position = originalPosition;
	}

}
