﻿using UnityEngine;
using System.Collections;

namespace Helper
{
    public class RotateTowards
    {
        /// <summary>
        /// Rotate from specified point to other, facing forward.
        /// Use with transform.rotation = RotateToward.Point(from, toward)
        /// </summary>
        /// <param name="from">From point.</param>
        /// <param name="towards">Towards point.</param>
        /// <param name="offsetAngle>Offset to add to calculation.</param> 
        public static Quaternion Point(Vector3 from, Vector3 towards, float offsetAngle) {
            // draw line from weapon to mouse
            Debug.DrawLine(from, towards);
            // calculate distance between those two
            Vector3 distance = towards - from;
            // calculate the angle it has to rotate
            float angle = Mathf.Atan2(distance.y, distance.x) * Mathf.Rad2Deg;
            // return new Quaternion
            return Quaternion.AngleAxis(angle + offsetAngle, Vector3.forward);
        }
        /// <summary>
        /// Rotate from specified point to other, facing forward.
        /// Use with transform.rotation = RotateToward.Point(from, toward)
        /// </summary>
        /// <param name="from">From point.</param>
        /// <param name="towards">Towards point.</param>
        /// <param name="axis">Rotational axis</param> 
        public static Quaternion Point(Vector3 from, Vector3 towards, Vector3 axis) {
            // draw line from weapon to mouse
            Debug.DrawLine(from, towards);
            // calculate distance between those two
            Vector3 distance = towards - from;
            // calculate the angle it has to rotate
            float angle = Mathf.Atan2(distance.y, distance.x) * Mathf.Rad2Deg;
            // return new Quaternion
            return Quaternion.AngleAxis(angle, axis);
        }
        /// <summary>
        /// Rotate from specified point to other, facing forward.
        /// Use with transform.rotation = RotateToward.Point(from, toward)
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="towards">Towards.</param>
        public static Quaternion Point(Vector3 from, Vector3 towards) {
            // draw line from weapon to mouse
            Debug.DrawLine(from, towards);
            // calculate distance between those two
            Vector3 distance = towards - from;
            // calculate the angle it has to rotate
            float angle = Mathf.Atan2(distance.y, distance.x) * Mathf.Rad2Deg;
            // return new Quaternion
            return Quaternion.AngleAxis(angle, Vector3.forward);
        }

        public static Quaternion Mouse(Vector3 from) {
            return RotateTowards.Point(from, Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }

        public static Quaternion Mouse(Vector3 from, Vector3 direction) {
            return RotateTowards.Point(from, Camera.main.ScreenToWorldPoint(Input.mousePosition), direction);
        }

        public static Vector3 RotateAroundPoint(Vector3 point, Vector3 pivot, Quaternion angle) {
            //		Vector3 finalPos = point - pivot;
            //		//Center the point around the origin
            //		finalPos = angle * finalPos;
            //		//Rotate the point.
            //
            //		finalPos += pivot;
            //		//Move the point back to its original offset. 
            //		
            //		return finalPos;
            return angle * (point - pivot) + pivot;
        }

        public static Vector3 CalculateCenteredMousePosition() {
            // offset is necessary, for centering the mouse position
            // could be precalculated, but would be problem, when screensize changes
            //BUG: switch middleOffsetcalulcation to delegate (when screensize changes only)
            Vector2 middleOffset = new Vector2(-Screen.width * 0.5f, -Screen.height * 0.5f);
            Vector2 centeredMousePosition;
            centeredMousePosition = Input.mousePosition;
            centeredMousePosition += middleOffset;
            centeredMousePosition.x /= Screen.width;
            centeredMousePosition.y /= Screen.height;

            //Debug.DrawLine(this.transform.position, new Vector2(this.transform.position.x, this.transform.position.y) + centeredMousePosition * 20.0f, Color.yellow);
            return centeredMousePosition;
        }

    }

}
