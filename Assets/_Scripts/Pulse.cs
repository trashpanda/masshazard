﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulse : MonoBehaviour {

    public float MaxSize = 2f;
    public float CurrentSize;
    public float ScaleSpeed = 0.2f;

    public float Opacity = 0f;
    public float CurrentOpacity;
    public float FadeSpeed = 0.2f;

    private SpriteRenderer sr;

    public bool isReady = false;

    void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    void FixedUpdate()
    {

        if (isReady)
        {
            CurrentSize -= ScaleSpeed * Time.deltaTime;
            CurrentOpacity += FadeSpeed * Time.deltaTime;

            if (CurrentSize < 1f)
            {
                CurrentSize = MaxSize;
                CurrentOpacity = 0f;
            }

            transform.localScale = new Vector3(CurrentSize, CurrentSize, 0);
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, CurrentOpacity);
        }
        
        
    }
	
}
