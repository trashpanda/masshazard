﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomCursor : MonoBehaviour {
	public Texture2D cursorTexture;
	CursorMode cursorMode  = CursorMode.Auto;
	Vector2 hotSpot = Vector2.zero;
	// Use this for initialization
	void Start () {
		//TODO: check if this actually works -- not sure if values are correct
		hotSpot = new Vector2(cursorTexture.width * 0.5f, cursorTexture.height * 0.5f);
		Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
	}
	void Update() {
	}

	void OnMouseEnter () {
		Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
		//Debug.Log("hello");
	}
	void OnMouseExit () {
		//Cursor.SetCursor(null, Vector2.zero, cursorMode);
	}
}

