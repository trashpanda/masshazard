﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FuelGuage : MonoBehaviour {
    public Slider Fuel;

    private Astronaut astro;

    void Awake()
    {
        GameObject Player = GameObject.FindGameObjectWithTag("Player");
        astro = Player.GetComponent<Astronaut>();

        Fuel = GetComponent<Slider>();
    }

    void FixedUpdate()
    {
        float amount = (astro.energy / 100f);
        Fuel.value = amount;
    }
	
}
