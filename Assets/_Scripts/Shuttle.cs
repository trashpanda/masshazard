﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shuttle : MonoBehaviour {

	public GameObject astronaut;
	public GameObject door;
	public SpriteRenderer shuttleRenderer;

	public Sprite shuttleClosed;
	public Sprite shuttleOpen;
	public Sprite shuttleFlying;

    public GameObject Win;

    void Awake()
    {
        astronaut = GameObject.FindGameObjectWithTag("Player");
    }

	// Use this for initialization
	void Start () {
		shuttleRenderer = this.GetComponent<SpriteRenderer>();
	}


	
	// Update is called once per frame
	void Update () {
	

	}

	void OnTriggerEnter2D(Collider2D other) {
		door.SetActive(true);
		shuttleRenderer.sprite = shuttleOpen;
        Win.SetActive(true);
    }
}
