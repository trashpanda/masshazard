﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceField : MonoBehaviour {

	public float fieldStrength = -10.0f;
	public bool  fieldActive = true;
	public bool  usePhysics = true;
	public bool  collideWithField = true;

	public float maxFieldStrength = -150.0f;

	public float propagationSpeed = 100.0f;

	// Use this for initialization
	void Start () {
		if (!collideWithField) {
			var collider = this.GetComponent<Collider2D>();
			if (collider)
				collider.enabled = false;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (Mathf.Abs(fieldStrength) > Mathf.Abs(maxFieldStrength))
			fieldStrength = maxFieldStrength;
		
		Vector3 fieldPosition = this.transform.position;

		if (fieldActive) {

			// find all attractables
			Attractable [] attractables = FindObjectsOfType<Attractable>();

			foreach (Attractable attr in attractables) {
				
				Vector3 attrPosition = attr.transform.position;

				Vector3 deltaPosition = attrPosition - fieldPosition;

				Vector3 gravityDelta = (deltaPosition * fieldStrength) / Mathf.Clamp(deltaPosition.sqrMagnitude, 0.001f, 1000.0f);
				//gravityDelta = Vector3.ClampMagnitude(gravityDelta, 1.0f);

				if (usePhysics) {
					Rigidbody2D rb = attr.GetComponent<Rigidbody2D>();

					rb.AddForce(gravityDelta);
				} else {
					attr.transform.position += (Vector3)gravityDelta;
				}

			}

		}

		var collider = this.GetComponent<Collider2D>();

		if (!collideWithField) {
			if (collider)
				collider.enabled = false;
		} else {
			if (collider)
				collider.enabled = true;
		}
			


	}
}
	