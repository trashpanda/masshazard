﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collector : MonoBehaviour {
   
    public Astronaut Astro;

    void Awake()
    {
        Astro = GetComponent<Astronaut>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<Collectable>())
        {
            Collectable c = other.gameObject.GetComponent<Collectable>();

            Astro.energy += c.EnergyAmount;
            Destroy(other.gameObject);
        }
    }
}
