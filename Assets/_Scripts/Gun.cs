﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {
	
	public SpriteRenderer gunRenderer;
	public Sprite nonFired;
	public Sprite fired;

	public void SetStateFired(bool isFired) {
		if (isFired)
			gunRenderer.sprite = fired;
		else
			gunRenderer.sprite = nonFired;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
