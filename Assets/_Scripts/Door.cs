﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

	public float speed = 2.0f;

	//public GameObject player;
	public FollowPlayerCamera camera;

	public Shuttle shuttle;
	private bool flyAway = false;
	private GameObject player;


	// Use this for initialization
	void Awake () {
		player = GameObject.FindGameObjectsWithTag("Player")[0];
	}
	
	// Update is called once per frame
	void Update () {

		if (flyAway) {
			//shuttle.transform.position.x += speed * Time.deltaTime;
			Vector3 delta = new Vector3(speed * Time.deltaTime, 0.0f, 0.0f);
			Vector3 newPosition = shuttle.transform.position + delta;
			shuttle.transform.position = newPosition;
		}
		
	}

	void OnTriggerEnter2D(Collider2D other) {
		Debug.Log("WOOOON!!!");
		shuttle.shuttleRenderer.sprite = shuttle.shuttleFlying;
		flyAway = true;

		//if (player)
			//= new Vector3(0.1, 0.1, 0.1f);

		player.SetActive(false);
	}
}
