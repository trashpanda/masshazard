﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public GameObject GameOver;
    public GameObject Win;

    public GameObject Player;
    private Astronaut astro;

    void Awake()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        astro = Player.GetComponent<Astronaut>();
    }


    void FixedUpdate()
    {
        if (astro.energy <= 0)
        {
            Time.timeScale = 0.0f;
            GameOver.SetActive(true);
        }

    }
}
