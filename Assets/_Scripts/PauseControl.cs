﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseControl : MonoBehaviour
{

    public GameObject pausePanel;

    public bool isPaused;
    // Use this for initialization
    void Start()
    {

        isPaused = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (isPaused)
        {
            PauseGame(true);
        }
        else
        {
            PauseGame(false);
        }

        if (Input.GetButtonDown("Cancel"))
        {
            SwitchPause();
        }
    }

    void PauseGame(bool state)
    {


        if (state)
        {
            Time.timeScale = 0.0f; // Pause
        }
        else
        {
            Time.timeScale = 1.0f; // Unpause
        }

        pausePanel.SetActive(state);
    }

    public void SwitchPause()
    {

        if (isPaused)
        {
            isPaused = false;
        }
        else
        {
            isPaused = true;
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ExitMenu(string menuScene)
    {
        SceneManager.LoadScene(0);
    }
}
