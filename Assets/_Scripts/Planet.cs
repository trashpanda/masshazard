﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour {

    public Material[] PlanetTextures;
    public int PlanetTextureNumber = 0;

    public float ScrollSpeed = 0.5f;
    public float MinScrollSpeed = 0.1f;
    public float MaxScroolSpeed = 0.6f;

    private float currentSpeed;

    private SpriteRenderer sr;

    public float MinRotation = -45f;
    public float MaxRotation = 45f;

    public float MinSize = 0.25f;
    public float MaxSize = 1f;

    public bool RandomGen = false;

    void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        if (RandomGen)
        {
            int maxPlanets = PlanetTextures.Length;
            int PlanetNum = Random.Range(0, maxPlanets);

            sr.material = PlanetTextures[PlanetNum];

            
        }
        else
        {
            sr.material = PlanetTextures[PlanetTextureNumber];
        }

        ScrollSpeed = Random.Range(MinScrollSpeed, MaxScroolSpeed);
        gameObject.transform.Rotate(new Vector3(0f, 0f, Random.Range(MinRotation, MaxRotation)));

        float size = Random.Range(MinSize, MaxSize);

        transform.localScale = new Vector3(size, size, 0);
    }

    void Update()
    { 
        currentSpeed += ScrollSpeed * Time.deltaTime;
        sr.material.SetTextureOffset("_ColorTex", new Vector2(currentSpeed, 0));
    }
	
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.name == "Player")
        {
            Astronaut astro = other.gameObject.GetComponent<Astronaut>();
            astro.energy -= 10f;
        }
    }
}
