﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astronaut : MonoBehaviour {

	public GameObject instaniate;
	public ParticleSystem particleSystem;

	public GameObject gun;

	public float energy = 100.0f;

	public float forceFieldSpawnStrength = -20.0f;

	public float forceFieldStepStrength = 1.0f;

	public bool deleteLastCreated = false;
	public float timeStep;
	private float currentTime = 0;

	private GameObject lastCreated;
	private bool createdForceField = false;

	public AudioClip fieldCharging;
	public AudioClip fieldGeneration;
	private AudioSource source;

	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource> ();

		particleSystem.Stop();
	}
	
	// Update is called once per frame
	void Update () {

		timeStep = fieldCharging.length/source.pitch;
		currentTime += Time.deltaTime;

		Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		mousePosition.z = 0;

		if (Input.GetMouseButtonDown(0)) {
			//Vector3 mousePos = new Vector3(Input.mousePosition.x * Screen.width, Input.mousePosition.y * Screen.height, 0.0f);
			//Vector3 mousePosition = // Input.mousePosition; //this.transform.position + Helper.RotateTowards.CalculateCenteredMousePosition();
			if (lastCreated && deleteLastCreated) {
				Object.Destroy(lastCreated);
			}
				
			lastCreated = Instantiate(instaniate, mousePosition, Quaternion.identity);
			var forceField = lastCreated.GetComponent<ForceField>();
			forceField.fieldStrength = forceFieldSpawnStrength;
			forceField.collideWithField = false;
			//GetComponent<Shaker>().PlayShake();

			createdForceField = true;
			particleSystem.Play();

			var gun = this.GetComponentInChildren<Gun>();
			if (gun)
				gun.SetStateFired(true);
			//if (gun)
			//	gun.GetComponent<Gun>().SetStateFired(true);
		}
		if (Input.GetMouseButtonUp(0)) {
			createdForceField = false;
			particleSystem.Stop();

			var gun = this.GetComponentInChildren<Gun>();
			if (gun)
				gun.SetStateFired(false);
			//if (gun)
			//	gun.GetComponent<Gun>().SetStateFired(false);
		}

		if (Input.GetMouseButton(0) && createdForceField) {
			var field = lastCreated.GetComponent<ForceField>();
			field.fieldStrength -= forceFieldStepStrength;
			energy -= forceFieldStepStrength * 0.05f;
		}

		if (Input.GetMouseButton (0)) 
		{
			var field = lastCreated.GetComponent<ForceField>();
			source.pitch = (-1f/160f)*field.fieldStrength +7f/8f;
			if (currentTime > timeStep) {
				source.PlayOneShot (fieldCharging, 0.2f);
				currentTime = 0;
			}
		}

		if (Input.GetMouseButtonUp(0))
		{
			source.pitch = 1;
			source.Stop ();
			source.PlayOneShot(fieldGeneration, 1f);
		}
				

		//if (Input.GetMouseButtonDown(1)) {
			
		//}


	}
}
