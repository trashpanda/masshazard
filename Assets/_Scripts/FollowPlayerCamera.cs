﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayerCamera : MonoBehaviour {

	public GameObject player;

	public float interpolationSpeed = 0.1f;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 playerPosition = player.transform.position;
		playerPosition.z = 0.0f;

		Vector3 camPosition = this.transform.position;
		camPosition.z = 0.0f;

		Vector3 deltaLerp = Vector3.Lerp(camPosition, playerPosition, interpolationSpeed * Time.deltaTime);
		deltaLerp.z = 0.0f;

		this.transform.position = deltaLerp;

		camPosition = this.transform.position;
		camPosition.z = -10.0f; 
		this.transform.position = camPosition;

	}
}
