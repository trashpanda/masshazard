﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowShip : MonoBehaviour {


    public GameObject Ship;

    void FixedUpdate()
    {
        Vector3 lookAtPos = Ship.transform.position;

        float rot_z = Mathf.Atan2(lookAtPos.y, lookAtPos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
    }
}
