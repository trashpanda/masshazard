﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulser : MonoBehaviour {

    public float MaxSize = 2f;
    public float ScaleSpeed = 0.2f;

    public float FadeSpeed = 0.4f;

    public int MaxPulses = 5;
    public GameObject pulse;

    void Start()
    {
        for (int i = 0; i < MaxPulses; i++)
        {

            GameObject p = Instantiate(pulse);
            p.transform.SetParent(transform);
            p.transform.position = transform.position;

            Pulse pScript = pulse.GetComponent<Pulse>();

            pScript.MaxSize = MaxSize;
            pScript.ScaleSpeed = ScaleSpeed;
            pScript.FadeSpeed = FadeSpeed;

            float startSize = MaxSize - (i * ScaleSpeed);
            pScript.CurrentSize = startSize;

            pScript.CurrentOpacity = i * FadeSpeed;
            pScript.isReady = true;
        }
    }
}
